package com.autoBot.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.autoBot.pages.LoginPage;
import com.autoBot.testng.api.base.Annotations;

public class TC002_CreateLeadTestCase extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC002_CreateLeadTestCase";
		testcaseDec = "Create Lead into leaftaps";
		author = "Vinoth";
		category = "SanityCheck";
		excelFileName = "TC002";
	} 

	@Test(dataProvider="fetchData") 
	public void createNewLead(String uName, String pwd) {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickCRMSFA()
		.clickLeadsTab()
		.clickCreateLead()
		.enterCompanyName()
		.enterFirstName()
		.enterLastName()
		.clickSubmitLead();
		
	}
	
}






