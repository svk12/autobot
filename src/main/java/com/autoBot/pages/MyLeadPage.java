package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;



public class MyLeadPage extends Annotations{ 
	
	public CreateLeadPage clickCreateLead() {
	WebElement eleCrLead = locateElement("link", "Create Lead");
	click(eleCrLead);
	return new CreateLeadPage();

	}
}







