package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;



public class ViewLeadPage extends Annotations{ 
	
	public ViewLeadPage verifyFirstName() {
		WebElement eleFname = locateElement("id", "viewLead_firstName_sp");
		verifyExactText(eleFname, "Vinoth");
		return this;
	}
}







