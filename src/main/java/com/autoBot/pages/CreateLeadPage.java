package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;



public class CreateLeadPage extends Annotations{ 
	
	public CreateLeadPage enterCompanyName() {
		WebElement eleCname = locateElement("id", "createLeadForm_companyName");
		clearAndType(eleCname, "iInterchange");
		return this; 

	}
	public CreateLeadPage enterFirstName() {
		WebElement eleFname = locateElement("id", "createLeadForm_firstName");
		clearAndType(eleFname, "Vinoth");
		return this;
	}
	public CreateLeadPage enterLastName() {
		WebElement eleLname = locateElement("id", "createLeadForm_lastName");
		clearAndType(eleLname, "Kumar");
		return this;
		
}
	public ViewLeadPage clickSubmitLead() {
		WebElement eleSub = locateElement("class", "smallSubmit");
		click(eleSub);
		return new ViewLeadPage();
	}
}






