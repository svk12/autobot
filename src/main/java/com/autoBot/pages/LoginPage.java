package com.autoBot.pages;

import org.openqa.selenium.WebElement;
import com.autoBot.testng.api.base.Annotations;

public class LoginPage extends Annotations{ 
	
	public LoginPage enterUserName(String data) {
		WebElement eleUserName = locateElement("id", "username");
		clearAndType(eleUserName, data);  
		return this; 
	}

	public LoginPage enterPassWord(String data) {
		WebElement elePassWord = locateElement("id", "password");
		clearAndType(elePassWord, data); 
		return this; 
	}

	public HomePage clickLogin() {
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);  
		return new HomePage();
	}

}







